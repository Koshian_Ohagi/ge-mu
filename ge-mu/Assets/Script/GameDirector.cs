﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameDirector : MonoBehaviour
{
    GameObject hpGauge;
    GameObject player;
    GameObject distance;
    int point = 0;

    void Start()
    {
        hpGauge = GameObject.Find("hpGauge");
        player = GameObject.Find("player");
        distance = GameObject.Find("Distance");
    }

    void Update()
    {
        
    }
    public void DecreaseHP()
    {
        hpGauge.GetComponent<Image>().fillAmount -= 0.2f;
    }


    public void PointCount()
    {
        point += 10000; //Gooditemにぶつかったらポイントを10追加
        distance.GetComponent<Text>().text = point + "えん";

        if(point==500000)
        {
            SceneManager.LoadScene("ClearScene");
        }
    }
}
