﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButitemGenerator : MonoBehaviour
{
    public GameObject Bullet;
    float span = 1.25f;
    float delta = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;
        if (delta > span)
        {
            delta = 0;
            GameObject go = Instantiate(Bullet) as GameObject;
            int px = Random.Range(-2, 2);
            go.transform.position = new Vector3(9, px, 0);
        }

    }
}
