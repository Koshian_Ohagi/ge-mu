﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Playercontroller : MonoBehaviour
{
    int Hit = 0;
    private Vector2 player_pos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(0, 0.1f, 0);
        }

        transform.Translate(0, -0.05f, 0); //重力


        player_pos = transform.position; //プレイヤーの位置を取得

        player_pos.y = Mathf.Clamp(player_pos.y, -2.0f, 1.5f); //移動範囲の指定
        transform.position = new Vector2(player_pos.x, player_pos.y); //新しい値の代入
    }

    public void GameOv()
    {
        Hit++; //ButitemとぶつかったらHitに+1

        if (Hit == 5) //もしHitが3になったらゲームオーバーシーンに飛ぶ
        {
            SceneManager.LoadScene("Gameovw");
        }
    }

  
}
