﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Butitemcontroller : MonoBehaviour
{
    public GameObject GOV;
    GameObject player;
   

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player"); 
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.13f, 0, 0);

        if (transform.position.x < -10.0f) //画面外に出たらアイテムの削除
        {
            Destroy(gameObject);
        }

        //当たり判定
        Vector2 p1 = transform.position;
        Vector2 p2 = player.transform.position;
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f;
        float r2 = 1.0f;

        if (d < r1 + r2)
        {
         

            Destroy(gameObject); //衝突したアイテムの削除


           

            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().DecreaseHP(); //GameDirectorに衝突したことを報告

            GameObject plyer = GameObject.Find("player");
            player.GetComponent<Playercontroller>().GameOv(); //playerに衝突したことを報告

        }
    }
}
